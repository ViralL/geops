'use strict';

var angl = {
  _0: '.clock__item--1',
  _36: '.clock__item--2',
  _72: '.clock__item--3',
  _108: '.clock__item--4',
  _144: '.clock__item--5',
  _180: '.clock__item--6',
  _216: '.clock__item--7',
  _252: '.clock__item--8',
  _288: '.clock__item--9',
  _324: '.clock__item--10'
};
var speedStep = false;

function detectAngl(ang) {
  for (var i = ang - 10; i < ang + 10; i++) {
    var i_t = i;
    if (i_t >= 360) {
      i_t -= 360;
    }
    if (i_t < 0) {
      i_t += 360;
    }
    if (angl['_' + i_t]) {
      // $(angl['_' + i_t]).addClass('active');
      /*var back = $(angl['_' + i_t]).data("back");
      $('.clock-img img').attr('src', back);

      $('.clock').css('background-image', 'url(' + back + ')');*/
    }
  }
}
function onStepFrame(ang) {
  // $('.clock__item--js').removeClass('active');
  detectAngl(ang);
}
// ready
$(document).ready(function () {

  // // canvas
  if ($('.clock').length) {
    var speedUp;
    var needSpeedUp;
    var ball;
    var ang;
    var fps;
    var herocanvas;
    var heroctx;
    var requestAnimFrame;
    var imgSprite;
    var next;
    var prev;

    (function () {
      var init = function init() {
        startloop();
      };

      var startloop = function startloop() {
        heroctx.save();
        clearherobg();
        // console.log(ang,speedStep,ang-speedStep,);
        heroctx.translate(herocanvas.width / 2, herocanvas.height / 2);
        heroctx.rotate((ang + speedStep) * Math.PI / 180);
        ang += speedStep;
        if (ang > 360) {
          ang = ang - 360;
        }
        if(ang<0){
          ang = 360 - ang;
        }
        if (fps != 30 && ang === speedUp) {
          fps = 30;
          speedStep = false;
          needSpeedUp = false;
        }
        onStepFrame(ang);
        if (needSpeedUp) {
          fps = 500;
          //speedStep = 2;
        }
        ball.draw();
        heroctx.restore();
        setTimeout(function () {
          requestAnimFrame(startloop);
        }, 1000 / fps);
      };

      var Hero = function Hero() {
        this.srcX = 0;
        this.srcY = 0;
        this.drawX = -235;
        this.drawY = -235;
        this.width = 470;
        this.height = 470;
      };

      var clearherobg = function clearherobg() {
        heroctx.clearRect(0, 0, 500, 500);
      };

      speedUp = 1;
      needSpeedUp = false;
      ball = new Hero();
      ang = 0;
      fps = 30;
      herocanvas = document.getElementById("myCanvas");

      herocanvas.width = 500;
      herocanvas.height = 500;
      heroctx = herocanvas.getContext('2d');
      requestAnimFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame;
      imgSprite = new Image();

      imgSprite.src = 'images/icons/arr-clock.png';
      imgSprite.addEventListener('load', init, false);

      Hero.prototype.draw = function () {
        heroctx.drawImage(imgSprite, this.drawX, this.drawY, this.width, this.height);
      };

      $('.clock__item--js').mouseenter(function () {
        var newAng = $(this).data("angl");
        var back = $(this).data("back");
        if(newAng === ang){
          return;
        }
        // if(ang === 0 && newAng>180){
        //   ang = 360;
        // }
        if(ang===360){
          ang = 0;
        }
        // var r = ang + 180;
        // if (r>360){
        //   r -=360;
        // }
        //
        // console.log('1: r = '+r,'newAng = '+newAng,'ang = ' + ang)
        var r = newAng - ang;
        if(r<0){
          r+=360;
        }
        if(r>180){
          speedStep = -2;
        } else{
          speedStep = +2;
        }
        // if(newAng>ang && r<newAng){
        //   r+=360;
        // }
        // if(newAng<ang && r>newAng){
        //   r-=360;
        // }
        // // var p_ang =  ang / newAng;
        //
        // console.log('2: r = '+r,'newAng = '+newAng,'ang = ' + ang)
        // if(newAng < r && newAng > ang){
        //   speedStep = +2;
        //   console.log(speedStep);
        // }
        // else{
        //   speedStep = -2;
        //   console.log(speedStep);
        // }

        $('.clock').css('background-image', 'url(' + back + ')');
        // $('.clock-img img').attr('src', back);
        speedUp = newAng;
        needSpeedUp = true;
      });
    })();
  }
  // // canvas

  function stateHover(mainClass, svgClass) {
    $(mainClass).hover(function () {
      $(svgClass).addClass("hover");
    }, function () {
      $(svgClass).removeClass("hover");
    });
  };
  stateHover(".chart__item--red", ".europe-red");
  stateHover(".chart__item--orange", ".europe-orange");
  stateHover(".chart__item--blue", ".europe-blue");
  stateHover(".chart__item--skyblue", ".europe-skyblue");
  stateHover(".chart__item--darkgreen", ".europe-darkgreen");
  stateHover(".chart__item--green", ".europe-green");

  // adaptive menu
  $('.main-nav__toggle--js').click(function () {
    $(this).parents().find('body').css('overflow', 'hidden');
    $(this).parents().find('.page-header__mobile').addClass('active');
  });
  $('.main-nav__close--js').click(function () {
    $(this).parents().find('body').css('overflow', 'auto');
    $(this).parent().removeClass('active');
  });
  $('.show-all--js').click(function () {
    $(this).hide('fast');
    $(this).parent().find('.reviews__item').slideDown();
  });

  // adaptive menu

  // mask phone {maskedinput}
  $("[name=phone]").mask("+7 (999) 999-9999");
  // mask phone

  // slider {slick-carousel}
  $('.slideshow').slick({
    infinite: true,
    prevArrow: '<div class="slick-arrow-left slick-arrow slick-arrow--bb"><i class="icon-arr-prev"></i></div>',
    nextArrow: '<div class="slick-arrow-right slick-arrow slick-arrow--bb"><i class="icon-arr-next"></i></div>'
  });
  $('.slider').slick({
    infinite: false,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow: '<div class="slick-arrow-left slick-arrow"><i class="is-icons is-icons--arr-prev"></i></div>',
    nextArrow: '<div class="slick-arrow-right slick-arrow"><i class="is-icons is-icons--arr-next"></i></div>',
    responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 480,
      settings: "unslick"
    }]
  });
  $('.slider-lg').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow: '<div class="slick-arrow-left slick-arrow slick-arrow--bb"><i class="is-icons is-icons--arr-prev"></i></div>',
    nextArrow: '<div class="slick-arrow-right slick-arrow slick-arrow--bb"><i class="is-icons is-icons--arr-next"></i></div>',
    responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: "unslick"
    }]
  });
  $('.slider-ph').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow: '<div class="slick-arrow-left slick-arrow slick-arrow--bb"><i class="is-icons is-icons--arr-prev"></i></div>',
    nextArrow: '<div class="slick-arrow-right slick-arrow slick-arrow--bb"><i class="is-icons is-icons--arr-next"></i></div>',
    responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $('.slider-vac').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
      breakpoint: 1480,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  // slider

  // select {select2}
  $("select").select2({
    minimumResultsForSearch: Infinity
  });
  // select

  // popup {magnific-popup}
  //$('.image-gallery').magnificPopup({});
  $('.zoom-gallery').each(function () {
    // the containers for all your galleries
    $(this).magnificPopup({
      delegate: 'a',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300, // don't foget to change the duration also in CSS
        opener: function opener(element) {
          return element.find('img');
        }
      }
    });
  });
  // popup

  $('.collapse-link--js').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    if ($this.next().hasClass('show')) {
      $this.next().removeClass('show');
      $this.removeClass('active');
      $this.next().slideUp(350);
    } else {
      $this.parent().parent().find('.collapse__item--js .collapse-inner').removeClass('show');
      $this.parent().parent().find('.collapse__item--js .collapse-inner').slideUp(350);
      $this.parent().parent().find('.collapse__item--js .collapse-link--js').removeClass('active');
      $this.next().toggleClass('show');
      $this.toggleClass('active');
      $this.next().slideToggle(350);
    }
  });
});
// ready

var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {
  $('.main-nav__item--js a.parent').click(function () {
    $(this).next().slideToggle();
    return false;
  });
}
//# sourceMappingURL=main.js.map
